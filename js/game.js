	//blocks
	var g_blocks = new Array([0,90,-1,0,0,0,1,0,0,-1],
							 [1,90,-2,0,-1,0,0,0,1,0],
							 [0,90,-1,0,0,0,1,0,1,-1],
							 [0,90,-1,0,-1,-1,0,0,1,0],
							 [0,0,-1,0,-1,-1,0,0,0,-1],
							 [1,90,0,0,1,0,-1,-1,0,-1],
							 [1,90,-1,0,0,0,0,-1,1,-1]);

	// variables for displayed block
	var g_blockIndex = 0;
	var g_block = new Array(8);
	var g_blockMap = new Array(8);
	var g_blockCurrentY = 0;
	var g_blockCurrentX = 0;
	var g_blockToggleState = 0;

	// variables for playing field
	var g_plyfMap;
	var g_origin = 5;
	var g_plyfMaxX = 9;
	var g_plyfMinX = 0;
	var g_plyfMaxY = 19;
	var g_plyfMinY = 0;

	// variables for game state
	var g_gameTimerRef;
	var g_dropTimerRef;
	var g_gamePaused=false;
	var g_dropMode=false;
	var g_dropHeight = 0;
	var g_gameRunning=false;
	var g_nextRandomBlockId = 0;
	var g_linesCleared = 0;
	var g_score = 0;
	var g_beginningLevel = 0;
	var g_currentLevel = 0;
	var g_maxLevel = 9;
	var g_finalSpeed = 100;
	var g_currentSpeed = 0;

	// variables for images
	var g_cubeImgWidth = 20;
	var g_cubeImgHeight = 20;



	// set event handler
	document.onkeydown = function () {updatePlyf()};


	// Kudos to Allen Rice :)
	function E(isDeep){
		if (window.event == "" || window.event == undefined){
			if (isDeep){
				return arguments.callee.caller.arguments.callee.caller.arguments.callee.caller.arguments[0];
			}else{
				return arguments.callee.caller.arguments.callee.caller.arguments.callee.arguments[0];
			}
		}
		return window.event;
	}


	function updatePlyf(){
		var e = E();

		if(g_gameRunning == true && e.keyCode==80){pauseGame(); return void(0);}
		if(g_gameRunning == false || g_dropMode == true || g_gamePaused == true){return void(0);}

		switch(e.keyCode){
			case 40:
				// down arrow
				moveBlock(0,-1);
				break;

			case 38:
				// up arrow
				rotateBlock();
				// Play Sound
				if(document.getElementById("SoundOn").checked) document.s2.play();
				break;

			case 37:
				//left arrow
				moveBlock(-1,0);
				break;

			case 39:
				//right arrow
				moveBlock(1,0);
				break;

			case 32:
				// space bar
				dropBlock();
				// Play Sound
				if(document.getElementById("SoundOn").checked) document.s1.play();
				break;
		}
	}



	function pauseGame()
	{
		if(g_gamePaused == false){
			g_gamePaused = true;
			document.getElementById('Pause').style.display = 'block';
			setPlyfOpacity(50);

		}else{
			g_gamePaused = false;
			document.getElementById('Pause').style.display = 'none';
			setPlyfOpacity(100);
		}
	}

	function setPlyfOpacity(opacity)
	{
		/*
			Set opacity level of playing field images and current block
		*/

		var x;
		var y;
		var o;
		var i;

		for(y=0;y<=19;y++){
			for(x=0;x<=9;x++){
				if(g_plyfMap[y][x]==1){
					o = document.getElementById('i' + x + y);
					o.style.filter = "alpha(opacity=" + opacity + ")";
					o.style.opacity = opacity;
				}
			}
		}

		for(i=0;i<g_blockMap.length;i+=2){
			o = document.getElementById('m' + i);
			o.style.filter = "alpha(opacity=" + opacity + ")";
			o.style.opacity = opacity;
		}
	}

	function newGame()
	{
		var y;

		// clear all game pictures
		clearPlayingField();

		// init g_plyfMap array
		g_plyfMap = new Array(g_plyfMaxY);
		for(y=g_plyfMinY;y<=g_plyfMaxY;y++){
			g_plyfMap[y] = new Array(g_plyfMaxX);
		}

		if(g_gamePaused) pauseGame();

		stopGame();
		startGame();
	}


	function gameOver(){
		var x;
		var y;

		stopGame();
		drawGameOver()
		if(document.getElementById("SoundOn").checked) document.s3.play();
		//alert('Game Over');
	}

	function drawGameOver()
	{
		for(y=0;y<=19;y++){
			for(x=0;x<=9;x++){
				document.getElementById('i' + x + y).src = 'images/gameover.jpg';
			}
		}
	}


	function newBlock(b)
	{
		/*
			Create a new block and position it at the top of the playing field.
				Check if there is room to position block and if not then game is over.
		*/

		var i;

		// copy original g_block values over to current g_block array
		for(i=2;i<g_blocks[b].length;i+=2){
			g_block[i-2] = g_blocks[b][i];
			g_block[i-2+1] = g_blocks[b][i+1];
		}

		// set current g_block
		g_blockIndex = b;

		// set current position (top of plyf)
		g_blockCurrentX = g_origin;
		g_blockCurrentY = g_plyfMaxY;

		// map g_block x,y values onto playing field x,y values
		g_block.mapPoints(g_blockMap);

		// check if room to place g_block on playing field
		if(isBlockBounding()){
			gameOver();
		}else{
			// show g_block
			setBlockImage(g_blockIndex);
			setBlockPosition();
		}
	}


	function clearPlayingField(){
		/*
			Set playing field images to blank.gif
		*/

		var x;
		var y;

		for(y=0;y<=19;y++){
			for(x=0;x<=9;x++){
				document.getElementById('i' + x + y).src = 'images/blank.gif';
			}
		}
	}

	function dispBlock(f){
		/*
			Hide/display the movable block
		*/

		if(f==0){
			document.getElementById('Block').style.display = 'none';
		}else{
			document.getElementById('Block').style.display= 'block';
		}
	}


	Array.prototype.mapPoints = function (targetObj)
	{
		/*
			Maps block x,y values to playing field x,y values
				Add method prototype to the array object so we
				can access any array collection easily
		*/

		var i;
		var cX = 0;
		var cY = 0;
		var maxY = 0;
		var newX = 0;
		var newY = 0;

		for(i=0;i<this.length;i+=2){
			maxY = Math.max(maxY, this[i+1]);
		}

		for(i=0;i<this.length;i+=2){
			cX = this[i];
			cY = this[i+1];

			newX = g_blockCurrentX + cX;

			if(cY==0){
				newY = g_blockCurrentY;
			}else if(cY<0){
				newY = g_blockCurrentY + cY;
			}else if(cY>0 && cY < maxY){
				newY = g_blockCurrentY + 1;
			}else if(cY>0 && cY == maxY){
				newY = g_blockCurrentY + maxY;
			}

			targetObj[i] = newX;
			targetObj[i+1] = newY;
		}
	}

	function isCollision(a)
	{
		/*
			Returns true if block  values go outside of playing field min
				 and max ranges or beyond a block in the playing field map
		*/

		var i;

		for(i=0;i<a.length;i+=2){
			if((a[i] < g_plyfMinX || a[i] > g_plyfMaxX) || (a[i+1] < g_plyfMinY || a[i+1] > g_plyfMaxY) || (g_plyfMap[a[i+1]][a[i]] == 1)){
				return true;
			}
		}
	}

	function isBlockBounding()
	{
		/*
			Checks bounding points of the current blocks x,y map
				with the playing field map (basically an enhanced
				version of isCollision())
		*/

		var i;

		for(i=0;i<g_blockMap.length;i+=2){
			if(g_blockMap[i+1] == g_plyfMinY){
				return true;
			}else if(g_plyfMap[g_blockMap[i+1] - 1][g_blockMap[i]] == 1){
				return true;
			}

		}
	}


	function checkLines()
	{
		/*
			Checks playing field map to see if we have lines to clear, if so, call clearLines()
		*/

		var lines = new Array();

		for(y=0;y<=19; y++){
			for(x=0;x<=9; x++){
				if(g_plyfMap[y][x] != 1){
					break;
				}
				if(x==9){
					lines.push(y);
				}
			}
		}

		if(lines.length>0){
			clearLines(lines);
			shiftLines(lines);
		}

		updateLineCount(lines.length);
		updateScore(lines.length);
		updateLevel();
	}


	function clearLines(lines)
	{
		/*
			Clears the lines in the playing field map and calls shiftLines()
		*/

		var y;
		var x;

		for(y=0;y<lines.length;y++){
			for(x=0;x<=9;x++){;
				g_plyfMap[lines[y]][x] = 0;
			}
		}
	}

	function shiftLines(lines)
	{
		/*
			Shifts incompleted blocks downward to fill the empty space where the completed lines used to be.
		*/

		var newY = 0;
		var x;
		var y;

		for(y=0;y<19;y++){
			for(n=0;n<lines.length;n++){
				if(lines[n] == y+n){
					newY += 1;
				}
			}

			for(x=0;x<=9;x++){
				try{
					g_plyfMap[y][x] = g_plyfMap[newY][x];
					document.getElementById('i' + x + y).src = document.getElementById('i' + x + newY).src;
				}catch(e){
					//alert(y + ', ' + newY);
				}
			}
			newY += 1;
		}
	}


	function rotateBlock()
	{
		/*
			Rotates a blocks x,y values and then re-maps them to playing field x,y values. Checks
				for collisions before updating block position.
		*/

		var i;
		var rad;
		var newXY = new Array(8);
		var newXYMap = new Array(8);

		rad = g_blocks[g_blockIndex][1] * -(Math.PI / 180);

		// determin if we should toggle the g_block between rotations, depending on block type
		if(g_blocks[g_blockIndex][0] == 1){
			if(g_blockToggleState == 1){
				g_blockToggleState = 0;
				rad = rad * -1;
			}else{
				g_blockToggleState = 1;
			}
		}

		// calculate rotation
		for(i=0;i<g_block.length;i+=2){
			newXY[i] = Math.floor(Math.cos(rad)) * g_block[i] - Math.ceil(Math.sin(rad)) * g_block[i+1];
			newXY[i+1] = Math.ceil(Math.sin(rad)) * g_block[i] + Math.floor(Math.cos(rad)) * g_block[i+1];
		}

		newXY.mapPoints(newXYMap);

		if(isCollision(newXYMap)){
			return void(0);
		}

		for(i=0;i<newXY.length;i+=2){
			g_block[i] = newXY[i];
			g_block[i+1] = newXY[i+1];
			g_blockMap[i] = newXYMap[i];
			g_blockMap[i+1] = newXYMap[i+1];
		}

		setBlockPosition();
	}


	function moveBlock(x,y)
	{
		/*
			Moves a block on the screen
		*/

		var i;
		var newXYMap = new Array(8);

		for(i=0;i<g_blockMap.length;i+=2){
			newXYMap[i] = g_blockMap[i] + x;
			newXYMap[i+1] = g_blockMap[i+1] + y;
		}

		if(isCollision(newXYMap)){
			return void(0);
		}

		g_blockCurrentX += x;
		g_blockCurrentY += y;

		for(i=0;i<g_blockMap.length;i+=2){
			g_blockMap[i] = newXYMap[i];
			g_blockMap[i+1] = newXYMap[i+1];
		}

		setBlockPosition();
	}

	function drawPlayingField()
	{
		/*
			Writes out the html that displays the playing field
		*/

		var x;
		var y;
		var newY;
		var currentTop = 0;
		var currentLeft = 0;

		for(y = 0; y <= g_plyfMaxY; y++){
			for(x = 0; x <= g_plyfMaxX; x++){
				newY = g_plyfMaxY - y;
				document.write('<div id=\"s' + x + newY + '\" class=\"cube\" style=\"left:' + currentLeft + '; top:' + currentTop + ';\"><img id=\"i' + x + newY +'\" src="images/blank.gif" alt=\"i' + x + newY +'\" width=\"20\" height=\"20\" class=\"i\" style=\"left:' + currentLeft + '; top:' + currentTop + ';\"></div>');
				currentLeft = currentLeft + 20;
			}
			document.write('<br>');
			currentTop = currentTop + 20;
			currentLeft = 0;
		}
	}

	function setBlockImage(b)
	{
		/*
			Updates block images
		*/

		var i;

		for(i=0;i<g_blockMap.length;i+=2){
			document.getElementById('i' + i).src = 'images/' + b + '.jpg';
		}
	}

	function setPlyfCubes()
	{
		/*
			Clones a block to the playing field
		*/

		var i;
		var o;

		for(i=0;i<g_blockMap.length;i+=2){
				g_plyfMap[g_blockMap[i+1]][g_blockMap[i]] = 1;
				o = document.getElementById('i' + g_blockMap[i] + g_blockMap[i+1])
				o.src = 'images/' + g_blockIndex + '.jpg';
		}
	}


	function setBlockPosition()
	{
		/*
			Sets the movable block's position on the playing field
		*/

		var i;

		for(i=0;i<g_blockMap.length;i+=2){
			document.getElementById('m' + i).style.left = document.getElementById('i' + g_blockMap[i] + g_blockMap[i+1]).style.left;
			document.getElementById('m' + i).style.top = document.getElementById('i' + g_blockMap[i] + g_blockMap[i+1]).style.top;
		}
	}


	function main()
	{
		/*
			Main game loop which iterates block downward movement.
		*/

		if(isBlockBounding()) {
			if(g_dropMode){
				g_dropMode = false;
				window.clearInterval(g_dropTimerRef);
			}
			setPlyfCubes();
			checkLines();
			newBlock(getNextBlock());
		}else{
			if(g_dropMode == false && g_gamePaused == false) {
				moveBlock(0,-1);
			}
		}
	}


	function updateScore(linesCleared)
	{
		/*
			Propose:	Updates score
		*/

		g_score += (g_currentLevel + 1) * (g_dropHeight + 1) * linesCleared;
		document.getElementById('Score').innerHTML = g_score;
	}


	function updateLineCount(linesCleared)
	{
		/*
			Propose:	Updates line count and calculates and resets speed
		*/

		g_linesCleared += linesCleared;
		document.getElementById('LineCount').innerHTML = g_linesCleared;
	}

	function updateLevel(reset){
		/*
			Propose:	Increments the current level variable
		*/

		var currentSpeed;

		if(reset) g_currentLevel = 0;
		if(g_currentLevel <= g_maxLevel){
			if(g_linesCleared > 32*(g_currentLevel+1)){
				//increment level
				g_currentLevel+=1;
				//find speed
				currentSpeed = CalcCurrentSpeed();
				//adjust drop speed
				window.clearInterval(g_gameTimerRef);
				//update speed
				g_gameTimerRef = window.setInterval("main()", currentSpeed);
			}
			document.getElementById('Level').innerHTML = g_currentLevel;
		}
	}

	function CalcCurrentSpeed()
	{
		return Math.floor(g_finalSpeed*(9-g_currentLevel+1)^1.0001);
	}

	function startGame()
	{
		/*
			Propose:	Initalizes and re-sets game variables,
						 starts interval timer and creates a new block
		*/

		// assign inital random g_block
		g_nextRandomBlockId = getRandomBlock();

		// set and display inital game values
		g_score = 0;
		g_linesCleared = 0;
		g_currentLevel = g_beginningLevel;

		updateScore(0);
		updateLineCount(0);
		updateLevel(true);

		// start game timer
		g_gameTimerRef = window.setInterval("main()", CalcCurrentSpeed());
		g_gameRunning = true;

		if(g_gamePaused==true){
			pauseGame();
		}

		//if(document.getElementById("SoundOn").checked) document.s4.play();

		// create new block
		newBlock(getNextBlock());
		dispBlock(1);
	}


	function stopGame()
	{
		/*
			Propose:	Stops game timer and clears screen
		*/

		dispBlock(0);
		document.getElementById('Preview').src = 'images/blank.gif';

		window.clearInterval(g_gameTimerRef);
		g_gameRunning = false;
	}

	function dropBlock()
	{
		/*
			Propose:	Starts an interval timer that will call a function to update block movement
		*/

		g_dropMode = true;
		g_dropHeight = g_blockCurrentY;
		g_dropTimerRef = window.setInterval("dropBlockCallback()", 1);
	}

	function dropBlockCallback()
	{
		/*
			Propose:	Callback function that callls moveBlock(0,-1) to move block downward.
		*/
		if(g_dropMode) moveBlock(0,-1);
	}


	function getRandomBlock()
	{
		/*
			Propose:	Generate a random number between 0 and 6
		*/

		return parseInt(Math.random() * 7);
	}


	function getNextBlock(){
		/*
			Propose:	Generates a new random block id and stores the value
						until getNextBlock() is called again as well as updates the
						preview block icon. Returns previously genreated block id.
			Returns:	Integer
		*/

		var thisRandomBlockId = 0;

		thisRandomBlockId = g_nextRandomBlockId;
		g_nextRandomBlockId = getRandomBlock();

		document.getElementById('Preview').src = 'images/p' + g_nextRandomBlockId + '.gif';

		return thisRandomBlockId;
	}