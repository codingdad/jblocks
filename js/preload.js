	var imageFiles = new Array('blank.gif','0.jpg','1.jpg','2.jpg','3.jpg','4.jpg','5.jpg','6.jpg','gameover.jpg','p0.gif','p1.gif','p2.gif','p3.gif','p4.gif','p5.gif','p6.gif');
	var imageObjects = new Array();
	var imagesLoaded = 0;

	function preloadImages()
	{
		/*
			Download all images used in game to browser cache on page load.
		*/

		var i;

		for(i=0;i<imageFiles.length;i++){
			imageObjects[i] = new Image;
			imageObjects[i].onload = function() {isLoaded()}
			imageObjects[i].onerror = function() {alert('Some images did not load properly. Please try refreshing the page.')}
			imageObjects[i].src = 'images/' + imageFiles[i];
		}
	}

	function isLoaded()
	{
		imagesLoaded++;
		if(imagesLoaded==imageFiles.length){
			document.getElementById('ui').style.display = 'block';
		}
	}